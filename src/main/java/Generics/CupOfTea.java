package Generics;

import Generics.Cup;

public class CupOfTea extends Cup<Tea> {
    public CupOfTea(Tea tea) {
        super(tea);
    }
}
