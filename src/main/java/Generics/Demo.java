package Generics;

public class Demo {
    public static void main(String[] args) {
        CupOfCoffee cupOfCoffee = new CupOfCoffee(new Coffee());
        CupOfTea cupOfTea = new CupOfTea(new Tea());

        cupOfCoffee.drink();
        cupOfTea.drink();

        Cup<Juice> cupOfJuice = new Cup<Juice>(new Juice());
        cupOfJuice.drink();

        cupOfJuice.fill(new Juice());
    }
}
